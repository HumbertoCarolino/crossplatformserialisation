#include "Bob.h"
#include "InputMemoryStream.h"
#include "OutputMemoryStream.h"


Bob::Bob()
{
	
	this->i = 3.14f;
	this->j = 4.25f;
	this->k =  5.15f;

	this->x = 10;
	this->y =  14;
	this->z = 16;

}

Bob::~Bob()
{

}

void Bob::setI(float i) 
{
	this->i = i;
}

float Bob::getI() 
{
	return this->i;
}

void Bob::setJ(float J)
{
	this->j = J;
}

float Bob::getJ()
{
	return this->j;
}

void Bob::setK(float K)
{
	this->k = K;
}

float Bob::getK()
{
	return this->k;
}


void Bob::setX(int X)
{
	this->x = X;
}

int Bob::getX()
{
	return this->x;
}

void Bob::setY(int Y)
{
	this->y = Y;
}

int Bob::getY()
{
	return this->y;
}

void Bob::setZ(int Z)
{
	this->z = Z;
}

int Bob::getZ()
{
	return this->z;
}


void Bob::Write(OutputMemoryStream& out) 
{
	out.Write(i); 
	out.Write(j); 
	out.Write(k);

	out.Write(x); 
	out.Write(y); 
	out.Write(z);

}


void Bob::Read(InputMemoryStream& in) 
{
	in.Read(i);
	in.Read(j);
	in.Read(k);

	in.Read(x);
	in.Read(y);
	in.Read(z);

}


bool Bob::operator==(Bob& other) 
{
	if (i != other.i) return false;
	if (std::abs(j - other.j) > 0.0000001) return false;


	return true;
}