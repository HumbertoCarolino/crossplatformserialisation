#pragma once


#include <cmath>
class InputMemoryStream;
class OutputMemoryStream;

class Bob
{
public:
	Bob();
	~Bob();

	void setI(float i);
	float getI(void);
	void setJ(float J);
	float getJ(void);
	void setK(float K);
	float getK(void);

	void setX(int i);
	int getX(void);
	void setY(int i);
	int getY(void);
	void setZ(int i);
	int getZ(void);



	void Write(OutputMemoryStream& out );
	void Read(InputMemoryStream& in);
	bool  operator==(Bob& other);


private:

	float i, j, k;
	int x, y, z;


};