
#include "InputMemoryStream.h"
#include "InputMemoryBitStream.h"

#include "OutputMemoryStream.h"
#include "OutputMemoryBitStream.h"
#include "Bob.h"
#include <iostream>

using std::cout;
using std::endl;

#if _WIN32
#include <Windows.h>
#endif

#include <memory>
using std::shared_ptr;


#if _WIN32
int main(int argc, const char** argv)
{
	Bob bob;
	Bob bob2;
	shared_ptr<OutputMemoryStream> out;
	shared_ptr<InputMemoryStream> in;

	out.reset(new OutputMemoryStream());
	bob.Write(*out);

	bob.setI(1);

	// Setup a buffer (out is your OutputMemoryStream)
	int copyLen = out->GetLength();
	char* copyBuff = new char[copyLen];

	// Copy over the buffer
	memcpy(copyBuff, out->GetBufferPtr(), copyLen);
	// Create a new memory stream
	in.reset(new InputMemoryStream(copyBuff, copyLen));

	bob2.Read(*in);

	std::cout << bob.operator==(bob2) << std::endl;



}
#else
const char** __argv;
int __argc;
int main(int argc, const char** argv)
{
	__argc = argc;
	__argv = argv;

}
#endif
